require("scripts/multiple_events")
require("scripts/features")
local game_manager = require("scripts/game_manager")

local start_menus = require("scripts/menus/start_menus")

-- This function is called when Solarus starts.
function sol.main:on_started()
  -- Define language.
  sol.language.set_language("en")

  -- Show start menus.
  start_menus:show({
    -- Add here your menus.
    "scripts/menus/solarus_logo"
  },
  function()
    -- Start games after start menus have been shown.
    -- You may also start a game from a menu, before.
    local game = game_manager:create("game_save.dat")
    sol.main:start_savegame(game)
  end)
end

-- Checks key presses for switching the fullscreen and terminating the game
--
-- key - is a string of the Name of a keyboard key
-- modifiers - is a table whose keys indicate what modifiers("shift", "control" and "alt") were down during the event.
--
-- Example
--  on_key_pressed(key => string, modifiers => table)
--  #=> boolian
--
-- Returns boolian
function sol.main:on_key_pressed(key, modifiers)
  local handled = false

  if (key == "return" and (modifiers.alt or modifiers.control)) then
    sol.video.set_fullscreen(not sol.video.is_fullscreen())
    handled = true

  elseif key == "f4" and modifiers.alt then
    sol.main.exit()
    handled = true
  end

  return handled
end

function sol.main:start_savegame(game)
  --local game = sol.game.load('savefile.dat')
  sol.main.game = game
  game:start()
end