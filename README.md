# MIT Starter Quest
This is a [Solarus Quest](https://www.solarus-games.org/) which cantains only [MIT](https://en.wikipedia.org/wiki/MIT_License) licensed scripts and [Public Domain](https://creativecommons.org/publicdomain/zero/1.0/) art assets. [Here](Art_Asset_List.txt) is the list of art assets used.

For more information on how the dialog boxes please check out the [Visual Novel System](https://gitlab.com/ShargonPendragon/visual-novel-system)

## Collaborate
1. Fork Repo and Clone it
2. Make Changes
3. Ensure the license is one of the following:
    Art: CC0, CC-BY
    Code: MIT
4. Fill in the Author and License fields in Solarus Quest Editor (resize the file tree if you can't find it)
5. Submit PR
